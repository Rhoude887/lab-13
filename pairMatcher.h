#pragma once

#include "charStack.h"

class PairMatcher {
private:
    char _openChar, _closeChar;
    CharStack charStack;
public:
  PairMatcher(char openChar, char closeChar);

  bool check(const std::string &testString);
};